terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 2.13.0"
    }
  }
}

provider "docker" {}

resource "docker_image" "wildfly" {
  name         = "wi11i4m/wildfly:1.0"
  keep_locally = false
}

resource "docker_container" "wildfly" {
  image = docker_image.wildfly.latest
  name  = "tutorial"
  ports {
    internal = 8080
    external = 9000
  }
}
