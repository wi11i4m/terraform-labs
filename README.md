# Terraform Labs

## Get Started with ***Terraform***

### Install terraform

> Ubuntu
```
$ curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
$ sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
$ sudo apt-get update && sudo apt-get install -y terraform
```
> Centos
```
$ sudo yum install -y yum-utils
$ sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/RHEL/hashicorp.repo
$ sudo yum -y install terraform
```

### Start using Terraform - ***Essentials Commands***

- [ ] Prepare your working directory for other commands
```
terraform init
```
- [ ] Format the terraform configuration file in a standart style (*.tf)
```
terraform fmt
```
- [ ] Deploy the infrastructure based on the terraform config file (*.tf)
```
terraform apply
```
- [ ] Display current state or saved plan
```
terraform show
```
- [ ] Delete the infrastructure deployed based on the terraform config file (*.tf)
```
terraform destroy
```